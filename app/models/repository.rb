class Repository < ApplicationRecord
  has_many :archives, dependent: :destroy

  def last_update
    self.archives
        .where(deleted_at: nil)
        .collect(&:created_at)
        .max
  end
end
