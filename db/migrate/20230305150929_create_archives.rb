class CreateArchives < ActiveRecord::Migration[7.0]
  def change
    create_table :archives, id: :string do |t|
      t.references :repository, null: false, foreign_key: true, type: :string
      t.string :name, null: false
      t.integer :size, limit: 8, null: false
      t.integer :real_size, limit: 8, null: false
      t.integer :duration, null: false
      t.datetime :created_at, null: false
      t.datetime :purged_at
    end
  end
end
